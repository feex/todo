/** @namespace Bootstrap */

/**
 * @name Bootstrap.start
 * @module
 */
import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'

import App from 'Layout/App'

import 'Style/index.scss'


render(
    <AppContainer>
        <App />
    </AppContainer>,
    document.getElementById('root')
)

if (module.hot) {
    module.hot.accept('Layout/App', () => {
        const NextApp = require('Layout/App').default

        render(
            <AppContainer>
                <NextApp />
            </AppContainer>,
            document.getElementById('root')
        )
    })
}
