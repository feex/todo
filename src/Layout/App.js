import React, { Component } from 'react'
import { observer } from 'mobx-react'
import DevTools from 'mobx-react-devtools'

import Dashboard from 'Layout/Dashboard'
import RTL from 'Layout/RTL'

import Timer from 'Layout/Timer'
import TimerState from 'Layout/Timer/TimerState'

import s from './App.scss'

const timerState = new TimerState()


@observer
class App extends Component {

    /**
     * TODO: Настроить отображение DevTools при __DEV__
     *
     * @returns {XML}
     */
    render() {
        return (
            <div className={s.container}>
                <Dashboard />
                <Timer timerState={timerState}/>

                <RTL />

                
                <DevTools />
            </div>
        )
    }
}

export default App
