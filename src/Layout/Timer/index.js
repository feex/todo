import _ from 'lodash'
import React, { Component } from 'react'
import { Button} from 'react-toolbox'
import { observer } from 'mobx-react'


@observer
class Timer extends Component {
    render() {
        return (
            <Button raised primary onClick={this.onReset}>
                Seconds passed: {this.props.timerState.timer}
            </Button>
        )
    }

    onReset = () => {
        this.props.timerState.resetTimer()
    }
}

export default Timer