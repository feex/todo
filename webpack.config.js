const path = require('path')
const argv = require('yargs').argv
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const autoprefixer = require('autoprefixer')

const CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin
const DefinePlugin = webpack.DefinePlugin

const __PROD__ = !!argv.production
const __DEV__ = (!__PROD__)
const __NOTIFIER__ = !!argv.notifier

/**
 * Base config.
 */
const webpackConfig = {
  devtool: 'eval',
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/Bootstrap/start'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  module: {
    loaders: [],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],

    modulesDirectories: [
      path.resolve(__dirname, './node_modules/'),
      path.resolve(__dirname, './src'),
    ],
  },
}

/**
 * Loaders.
 * TODO: Настроить ExtractTextPlugin и генерацию стилей для продакшена
 */
const extractAppCss = new ExtractTextPlugin('[name].css')
// const stylusLoaderConfig = 'css?-autoprefixer&sourceMap!postcss!stylus?resolve url'
// const sassLoaderConfig = 'css?-autoprefixer&sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]!postcss!sass'
// const sassLoaderConfig = 'css?-autoprefixer&sourceMap!postcss!sass'

// 'css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass'
// "sass-loader?indentedSyntax=sass&sourceMap=map&includePaths[]=" + path.resolve(__dirname, "./public/bundle/"),

/**
 * PreLoaders
 * @type {Array}
 */
webpackConfig.module.preLoaders = []

/**
 * Loaders
 * @type {{loaders: *[]}}
 */
webpackConfig.module.loaders = [
  {
    test: /\.jsx?$/,
    loaders: ['babel'],
    include: path.join(__dirname, 'src')
  },
  {
    test: /\.json/,
    loader: 'json-loader',
  },
  {
    test: /\.(stylus)$/,
    loader: 'style!css?-autoprefixer&sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]!postcss!stylus?resolve url',
  },
  {
    test: /\.(sass|scss)$/,
    loader: 'style!css?-autoprefixer&modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]!postcss!sass?sourceMap=map',
  },
]

/**
 * File Loaders
 * TODO: Настроить loaders для файлов
 */
webpackConfig.module.loaders.push([
  {
    test: /\.woff(\?.*)?$/,
    loader: 'url?prefix=fonts/&name=[path][name].[ext]?[hash]&limit=10000&mimetype=application/font-woff'
  },
  {
    test: /\.woff2(\?.*)?$/,
    loader: 'url?prefix=fonts/&name=[path][name].[ext]?[hash]&limit=10000&mimetype=application/font-woff2'
  },
  {
    test: /\.otf(\?.*)?$/,
    loader: 'file?prefix=fonts/&name=[path][name].[ext]?[hash]&limit=10000&mimetype=font/opentype'
  },
  {
    test: /\.ttf(\?.*)?$/,
    loader: 'url?prefix=fonts/&name=[path][name].[ext]?[hash]&limit=10000&mimetype=application/octet-stream'
  },
  {
    test: /\.eot(\?.*)?$/,
    loader: 'file?prefix=fonts/&name=[path][name].[ext]?[hash]'
  },
  {
    test: /\.svg(\?.*)?$/,
    loader: 'url?prefix=fonts/&name=[path][name].[ext]?[hash]&limit=10000&mimetype=image/svg+xml'
  },
  {
    test: /\.(png|jpg)$/,
    loader: 'url?prefix=file/&name=[path][name].[ext]?[hash]&limit=8192'
  },
  {
    test: /\.(ico)$/,
    loader: 'file?name=[name].[ext]?[hash]',
  }
])

/**
 * PostCSS Config
 * @returns {*[]}
 */
webpackConfig.postcss = function () {
  return [autoprefixer({ browsers: ["last 1 version", "last 10 Chrome versions", "> 5%"] })]
}

/**
 * Sass Config
 * @type {{data: string, includePaths: *[]}}
 */
webpackConfig.sassLoader = {
  data: '@import "Style/_config.scss";',
  includePaths: [path.resolve(__dirname, './src')]
}

/**
 * Config for Stylus
 */
// webpackConfig.stylus = {
//   import: [
//     '~Style/_config.styl',
//   ],
// }

/**
 * Plugins.
 * TODO: Настроить глобальные переменные окружения
 */
// webpackConfig.plugins = [
//   extractAppCss,
//   new CommonsChunkPlugin({ name: 'vendor', minChunks: detectThirdPartyModule }),
//
//   new HtmlWebpackPlugin({
//     template: 'src/index.html',
//     filename: 'index.html',
//     inject: 'body',
//   }),
//
//   new DefinePlugin({
//     __TEST__: false,
//     __PROD__,
//     __DEV__,
//   })
// ]
//
// function detectThirdPartyModule(module) {
//   return module.resource && (
//           /bower_components/.test(module.resource) ||
//           /node_modules/.test(module.resource)
//       )
// }

if (__PROD__) {
  webpackConfig.plugins.push(
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        drop_console: true,
        warnings: true,
        unsafe: false,
      })
  )
}

/**
 * Notifier
 */
if (__NOTIFIER__) {
  const WebpackBuildNotifierPlugin = require('webpack-build-notifier')

  webpackConfig.plugins.push(
      new WebpackBuildNotifierPlugin({ sound: false })
  )
}

module.exports = webpackConfig